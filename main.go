package main

import "fmt"

//var can be used to declare variables outside of functions short declaration := will not work.
//Declare there is a VARIABLE with the IDENTIFIER "global"
//And that the VARIABLE with the IDENTIFIER of "global" is of TYPE string
//ASSIGNS the ZERO VALUE of TYPE string to "global" which in strings case is ""
var global string

//Best practise limit the scope of the variable and use shorthand declaration wherever possible.
func main() {
	x := 12
	fmt.Println(x)

	x = 450
	fmt.Println(x)

	y := "Joe"
	fmt.Println("Hello ", y)

	global = "I'm declared outside of function"
	fmt.Println(global)

}
